<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $appends = ['name_slug'];   

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug'
    ]; 


    public function getNameSlugAttribute() {
    	return $this->name . ' ' . $this->slug;
    } 


    public function toArray() {
        $data = parent::toArray(); 
        return $data;
    } 
 
}
