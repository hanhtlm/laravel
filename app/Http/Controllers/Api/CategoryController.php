<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CategoryRepository;
use App\Traits\ApiTrait;
use App\Http\Requests\CategoryRequest;
use App\Category;
class CategoryController extends Controller
{
	use ApiTrait;
	public $category;
	function __construct(CategoryRepository $category) {
		$this->category = $category;

	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $category = $this->category->all();
        return $this->apiOk($category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //

        // $validator = Validator::make($req->all(), [
        //     'email' => 'required|email',
        //     'password' => 'required',
        // ]);

        // if ($validator->fails()) {
        //     return redirect(route('web.member.login'))
        //         ->with(['errors' => $validator->errors(),])
        //         ->withInput($req->except('password'));
        // }

        $validated = $request->all();  
        $create = $this->category->create($request->all()); 
        if($create) {
            return $this->apiOk(['mes' => 'Tạo thành công!'], 201);
        }
        else {
            $this->apiErrorWithCode(['mes' => 'Tạo không thành công!'], 422);

        }
        
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        //
        $validated = $request->all();  
        $update = $this->category->update($id,$request->all()); 
        if($update) {
            return $this->apiOk(['mes' => 'Update thành công!'], 202);
        }
        else {
            $this->apiErrorWithCode(['mes' => 'Update không thành công!'], 422);
 
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
