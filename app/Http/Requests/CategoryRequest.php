<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Contracts\Validation\V;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Traits\ApiTrait;
use Illuminate\Http\Request;
class CategoryRequest extends FormRequest
{
    use ApiTrait;

    public function __construct() {
        
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
      
        if ($this->method() == 'PUT') {
            return [
                'name' =>  'required|unique:categories,name,'.$this->route('category')
            ]; 
        }
        else {
            return [
                'name' => 'required|unique:categories'
            ]; 
        }
        
        
        
    }


    /**
     * Get the validation message that apply to the request.
     *
     * @return array
     */

    public function messages() 
    {
        return [
            'name.required' => trans('message.namecategory_required'),
           
        ];
    }


    /**
    * [failedValidation [Overriding the event validator for custom error response]]
    * @param  Validator $validator [description]
    * @return [object][object of various validation errors]
    */
    public function failedValidation(Validator $validator) { 
        throw new HttpResponseException($this->apiErrorWithCode($validator->errors(), 422)); 

    
       
   }

  

    
    

    
}
