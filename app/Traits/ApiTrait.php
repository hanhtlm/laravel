<?php

namespace App\Traits;

trait ApiTrait
{
	public function apiRes($status, $message, $data, $error, $code) {
			$apiResp = [];
	    $apiResp['success'] = $status;
	    $apiResp['data'] = $data;
	    $apiResp['msg'] = $message;
	    $apiResp['errors'] = $error;
    	return response($apiResp, $code ? $code : ($status ? 200 : 500));

	}  
	public function apiOk($data, $code = 200) {
		return $this->apiRes($code, null, $data, null, $code);
		
	} 

	public function apiError($msg)
	{
	    return $this->apiRes(false, $msg, null, null, null);
	}

	public function apiErrorWithCode($msg, $code)
	  {
	    return $this->apiRes(false, $msg, null, null, $code);
	  }
	  public function apiErrorDetails($msg, $errors, $code)
	  {
	    return $this->apiRes(false, $msg, null, $errors, $code);
	  }

}