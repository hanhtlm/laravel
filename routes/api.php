<?php
use Illuminate\Http\Request;
use \App\Http\Controllers;
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user')->middleware('scopes:scope-1');
    });
});
Route::group([
 'prefix' => 'authjwt'
], function() {
	Route::post('register', 'UserController@register');
	Route::post('login', 'UserController@login');
	Route::group(['middleware' => 'jwt.auth'], function () {
	    Route::post('user-info', 'UserController@getUserInfo');
	});


});

Route::resource('category', 'Api\CategoryController');

